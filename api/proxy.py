from flask import Flask
from urllib.request import urlopen
import redis

app = Flask(__name__)

i = 0

@app.route('/')
def add_to_list():
    global i
    i += 1
    r = redis.Redis(host='redis', 
                    encoding='utf-8', 
                    decode_responses=True)
    r.lpush('list', str(i))
    return ', '.join(r.lrange('list', 0, -1))
